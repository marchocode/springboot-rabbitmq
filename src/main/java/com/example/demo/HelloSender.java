package com.example.demo;

import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Date;

@Component
public class HelloSender {

    @Autowired
    private AmqpTemplate rabbitTemplate;

    public void send() {

        String [] keys = new String[]{"red","yellow","blue"};

        for (int i = 0;i<9;i++) {

            int random = (int)(Math.random()*3);
            String key = keys[random]+"."+keys[random];
            System.out.println(key);

            this.rabbitTemplate.convertAndSend("myfanout",key,key);
        }
    }

}