package com.example.demo;

import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

@Component
@RabbitListener(queues = "world")
public class ReceiverB {
    @RabbitHandler
    public void process(String hello) {
        System.out.println("ReceiverB  : " + hello);
    }
}
