package com.example.demo;

import org.springframework.amqp.core.*;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class RabbitConfig {

    @Bean
    public Queue helloQueue() {
        return new Queue("hello");
    }

    @Bean
    public Queue worldQueue() {
        return new Queue("world");
    }

    /**
     * 路由交换机
     */
//    @Bean
//    public DirectExchange exchange() {
//        return new DirectExchange("myexchange");
//    }

    /**
     * 匹配交换机
     * @return
     */
//    @Bean
//    public TopicExchange exchange(){
//        return new TopicExchange("mytopic");
//    }

    /**
     * 广播交换机
     * @return
     */
    @Bean
    public FanoutExchange exchange(){
        return new FanoutExchange("myfanout");
    }

//    @Bean
//    public Binding bindingExchangeHello(Queue helloQueue, DirectExchange exchange) {
//        return BindingBuilder.bind(helloQueue).to(exchange).with("red");
//    }

    /**
     * 匹配性交换机绑定
     * @param helloQueue
     * @param exchange
     * @return
     */
//    @Bean
//    public Binding bindingExchangeHello(Queue helloQueue, TopicExchange exchange) {
//        return BindingBuilder.bind(helloQueue).to(exchange).with("red.#");
//    }

//    @Bean
//    public Binding bindingExchangeWorld(Queue worldQueue, DirectExchange exchange) {
//        return BindingBuilder.bind(worldQueue).to(exchange).with("blue");
//    }

    /**
     * 匹配性交换机绑定
     * @param worldQueue
     * @param exchange
     * @return
     */
//    @Bean
//    public Binding bindingExchangeWorld(Queue worldQueue, TopicExchange exchange) {
//
//        return BindingBuilder.bind(worldQueue).to(exchange).with("blue.blue");
//    }

//    @Bean
//    public Binding bindingExchangeWorld2(Queue worldQueue, DirectExchange exchange) {
//        return BindingBuilder.bind(worldQueue).to(exchange).with("yellow");
//    }

    /**
     * 订阅性交换机绑定
     * @param helloQueue
     * @param exchange
     * @return
     */
    @Bean
    public Binding bindingExchangeHello(Queue helloQueue, FanoutExchange exchange) {
        return BindingBuilder.bind(helloQueue).to(exchange);
    }

    /**
     * 订阅性交换机绑定
     * @param worldQueue
     * @param exchange
     * @return
     */
    @Bean
    public Binding bindingExchangeWorld(Queue worldQueue, FanoutExchange exchange) {
        return BindingBuilder.bind(worldQueue).to(exchange);
    }

}